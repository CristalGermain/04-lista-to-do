const argv = require('./config/yargs').argv;
const porHacer = require('./por-hacer/por-hacer');
const colors = require('colors');


let comando = argv._[0];

switch (comando) {
    case 'crear':
        let tarea = porHacer.crear(argv.descripcion);
        break;
    case 'actualizar':
        if (porHacer.actualizar(argv.descripcion, argv.completado))
            console.log('La tarea ha sido completada');
        else
            console.log('La tarea no pudo actualizarse');
        break;
    case 'listar':

        let listado = porHacer.getListado();
        console.log('Lista de pendientes\n'.green);
        for (let tarea of listado) {
            console.log(tarea.descripcion);
            console.log('Estado: ', tarea.completado, '\n');
        }

        break;
    case 'borrar':
        if (porHacer.borrar(argv.descripcion))
            console.log('La tarea ha sido borrada con éxito');
        else
            console.log('No se pudo borrar la tarea');

        break;
    default:
        console.log('Comando no reconocido');
}