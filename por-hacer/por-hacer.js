const fs = require('fs');

let listadoPorHacer = [];

const guardarDB = (listado) => {
    let data = JSON.stringify(listado);
    fs.writeFile('./db/data.json', data, (err) => {
        if (err) throw new Error('No se pudo guardar en la db', err);

    });
};

const cargarDB = () => {
    try {
        // El require directo del archivo automaticamente deserializa el json
        listadoPorHacer = require('../db/data.json');
    } catch (error) {
        listadoPorHacer = [];
    }

};

const crear = (descripcion) => {

    cargarDB();
    let porHacer = {
        descripcion,
        completado: false
    };
    listadoPorHacer.push(porHacer); //Inserta este objeto al arreglo
    try {
        guardarDB(listadoPorHacer);
        console.log("La tarea ha sido guardada con éxito");
        return porHacer;
    } catch (error) {
        return null;
    }


};

const getListado = () => {
    cargarDB();
    return listadoPorHacer;
};

const actualizar = (descripcion, estado) => {

    cargarDB();
    //findIndex es un callback que regresa el index del elemento que cumple con la condición dada
    let index = listadoPorHacer.findIndex(tarea => {
        return tarea.descripcion === descripcion;
    });

    if (index >= 0) {
        listadoPorHacer[index].completado = true;
        guardarDB(listadoPorHacer);
        return true;
    } else
        return false;


};

const borrar = (descripcion) => {
    cargarDB()

    let nuevoListado = listadoPorHacer.filter(tarea => {
        return tarea.descripcion !== descripcion;
    });

    if (nuevoListado.length > 0) {
        guardarDB(nuevoListado);
        return true;
    } else
        return false;
};

module.exports = {
    crear,
    getListado,
    actualizar,
    borrar
}