## Aplicación de comandos

Ejercicio del curso
Lista de tareas por hacer: crea tareas, las enlista y actualiza o borra las tareas indicadas.

Aquí se utiliza un archivo JSON para almacenar datos y evitar que el programa sea volátil.
Se utilizan comandos mediante el paquete de Yargs y se utiliza el paquete FileStream para generar el archivo JSON.

El usuario inserta los datos mediante los comandos y así interactúa con el programa.


Instala los paquetes necesarios con el comando:
```
npm install

```